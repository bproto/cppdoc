# Copyright 2021-2024 Bytes Mess <b110011@pm.me>.
#
# CppDoc: CMake Module for an easy generation of C++ documentation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#
# User supplied paths
#

if(NOT CPPDOC_BINARY_DIR)
    message(FATAL_ERROR "'CPPDOC_BINARY_DIR' variable is not set or empty!")
endif()

if(NOT CPPDOC_SOURCE_DIR)
    message(FATAL_ERROR "'CPPDOC_SOURCE_DIR' variable is not set or empty!")
endif()

if(NOT CPPDOC_PY_MODULES_BINARY_DIR)
    set(CPPDOC_PY_MODULES_BINARY_DIR "${CPPDOC_BINARY_DIR}/python_modules")
endif()

# Doxygen

if(NOT CPPDOC_TAGFILE)
    set(_doxygen_tagfile "cppreference-doxygen-web-tag.xml=http://en.cppreference.com/w/")
    set(CPPDOC_TAGFILE "${CMAKE_CURRENT_LIST_DIR}/doxygen/${_doxygen_tagfile}")
endif()

# Sphinx general configuration

if(NOT CPPDOC_SPHINX_EXTENSIONS)
    set(CPPDOC_SPHINX_EXTENSIONS "")
endif()

# Sphinx options for internationalization

if(NOT CPPDOC_SPHINX_LANGUAGE)
    set(CPPDOC_SPHINX_LANGUAGE "en")
endif()

# Sphinx options for HTML output

if(CPPDOC_SPHINX_FURO_GITLAB_ICONS)
    set(CPPDOC_SPHINX_FURO_GITLAB_ICONS "True")
else()
    set(CPPDOC_SPHINX_FURO_GITLAB_ICONS "False")
endif()

if(CPPDOC_SPHINX_HTML_SHOW_SOURCELINK)
    set(CPPDOC_SPHINX_HTML_SHOW_SOURCELINK "True")
else()
    set(CPPDOC_SPHINX_HTML_SHOW_SOURCELINK "False")
endif()

if(NOT CPPDOC_SPHINX_HTML_THEME)
    set(CPPDOC_SPHINX_HTML_THEME "alabaster")
endif()

# Sphinx project info

if(NOT CPPDOC_SPHINX_PROJECT_NAME)
    set(CPPDOC_SPHINX_PROJECT_NAME "${PROJECT_NAME}")
endif()

if(NOT CPPDOC_SPHINX_PROJECT_AUTHOR)
    set(CPPDOC_SPHINX_PROJECT_AUTHOR "${CPPDOC_SPHINX_PROJECT_NAME} Developers")
endif()

if(NOT CPPDOC_SPHINX_PROJECT_COPYRIGHT)
    set(CPPDOC_SPHINX_PROJECT_COPYRIGHT "{}, ${CPPDOC_SPHINX_PROJECT_NAME}")
endif()

if(NOT CPPDOC_SPHINX_PROJECT_VERSION)
    set(CPPDOC_SPHINX_PROJECT_VERSION "${PROJECT_VERSION}")
endif()

#------------------------------------------------------------------------------#
# Checks                                                                       #
#------------------------------------------------------------------------------#

function(_cppdoc_check_doxygen)
    find_package(Doxygen)
    if(NOT DOXYGEN_FOUND)
        message(FATAL_ERROR "Unable to find 'doxygen' executable which is required by 'cppdoc' package!")
    endif()
endfunction()

_cppdoc_check_doxygen()

#------------------------------------------------------------------------------#
# Breathe + Doxygen + Exhale                                                   #
#------------------------------------------------------------------------------#

# This is a "meta" target that is used to collect all C++ information.
add_custom_target(cppdoc-meta-info)

set_target_properties(cppdoc-meta-info
    PROPERTIES
        # User supplied paths
        BINARY_DIR "${CPPDOC_BINARY_DIR}"
        SOURCE_DIR "${CPPDOC_SOURCE_DIR}"
        PY_MODULES_BINARY_DIR "${CPPDOC_PY_MODULES_BINARY_DIR}"
        # Doxygen
        TAGFILE "${CPPDOC_TAGFILE}"
        # Sphinx general configuration
        SPHINX_EXTENSIONS "${CPPDOC_SPHINX_EXTENSIONS}"
        # Sphinx options for internationalization
        SPHINX_LANGUAGE "${CPPDOC_SPHINX_LANGUAGE}"
        # Sphinx options for HTML output
        SPHINX_FURO_GITLAB_ICONS "${CPPDOC_SPHINX_FURO_GITLAB_ICONS}"
        SPHINX_HTML_SHOW_SOURCELINK "${CPPDOC_SPHINX_HTML_SHOW_SOURCELINK}"
        SPHINX_HTML_THEME "${CPPDOC_SPHINX_HTML_THEME}"
        # Sphinx project info
        SPHINX_PROJECT_AUTHOR "${CPPDOC_SPHINX_PROJECT_AUTHOR}"
        SPHINX_PROJECT_COPYRIGHT "${CPPDOC_SPHINX_PROJECT_COPYRIGHT}"
        SPHINX_PROJECT_NAME "${CPPDOC_SPHINX_PROJECT_NAME}"
        SPHINX_PROJECT_VERSION "${CPPDOC_SPHINX_PROJECT_VERSION}"
        # Library hardcoded paths
        CMAKE_MODULES_DIR "${CMAKE_CURRENT_LIST_DIR}/cmake"
        SPHINX_FILES_DIR "${CMAKE_CURRENT_LIST_DIR}/sphinx"
)

function(_cppdoc_get_value _prop)
    set(options ESCAPE)
    set(oneValueArgs)
    set(multiValueArgs)
    cmake_parse_arguments(opt "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(output "${_prop}_var")

    get_target_property("${output}" cppdoc-meta-info "${_prop}")
    if(opt_ESCAPE)
        set(escaped_list)
        foreach (value IN LISTS ${output})
            if(value) # Add only non-empty strings.
                list(APPEND escaped_list "\'${value}\'")
            endif()
        endforeach()
        set("${output}" ${escaped_list})
    endif()

    list(JOIN "${output}" ",\n    " "${output}")

    if("${${output}}" STREQUAL "${output}-NOTFOUND")
        message(FATAL_ERROR "Unable to get '${_prop}' property from 'cppdoc-meta-info' target.")
    endif()

    set("CPPDOC_${_prop}" "${${output}}" PARENT_SCOPE)
endfunction()

function(_cppdoc_create_breathe_projects_depends_prop _target)
    get_target_property(target_type "${_target}" TYPE)

    set(prop "BREATHE_PROJECTS_DEPENDS")
    if(target_type STREQUAL "INTERFACE_LIBRARY")
        set(prop "INTERFACE_${prop}")
    endif()

    set(BREATHE_PROJECTS_DEPENDS_PROP "${prop}" PARENT_SCOPE)
endfunction()

function(cppdoc_add_cpp_files_dir _target)
    set(options)
    set(oneValueArgs SOURCES)
    set(multiValueArgs DEPENDS)
    cmake_parse_arguments(opts "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    _cppdoc_get_value(BINARY_DIR)

    # BREATHE

    set_property(
        TARGET
            cppdoc-meta-info
        APPEND
            PROPERTY
                BREATHE_PROJECTS "'${_target}': '${CPPDOC_BINARY_DIR}/doxygen/${_target}/xml'"
    )
    set_property(
        TARGET
            cppdoc-meta-info
        APPEND
            PROPERTY
                BREATHE_PROJECTS_SOURCE "'${_target}': '${opts_SOURCES}'"
    )

    _cppdoc_create_breathe_projects_depends_prop("${_target}")

    set_property(
        TARGET
            "${_target}"
        APPEND
            PROPERTY
                "${BREATHE_PROJECTS_DEPENDS_PROP}" "${opts_DEPENDS}"
    )
    set_property(
        TARGET
            cppdoc-meta-info
        APPEND
            PROPERTY
                CMAKE_PROJECTS "${_target}"
    )

  # EXHALE

    set(args "'${_target}': {\n\
        'containmentFolder': '${CPPDOC_BINARY_DIR}/src/${_target}',\n\
        'doxygenStripFromPath': '${opts_SOURCES}'\n\
    }"
    )
    set_property(
        TARGET
            cppdoc-meta-info
        APPEND
            PROPERTY
                EXHALE_PROJECTS_ARGS ${args}
    )

    # Linting

    if(CMAKE_CXX_COMPILER_ID MATCHES ".*Clang")
        target_compile_options("${_target}" INTERFACE -Wdocumentation)
    endif()
endfunction()

macro(cppdoc_add_pyi_files_dir _py_module_name _target _dir)
    set(copy_target "cppdoc-copy-${_target}-pyi-files")
    set(gen_target "cppdoc-generate-${_target}-rst-files")

    _cppdoc_get_value(BINARY_DIR)
    _cppdoc_get_value(PY_MODULES_BINARY_DIR)

    set(py_module_dest_dir "${CPPDOC_PY_MODULES_BINARY_DIR}/${_py_module_name}")
    set(rst_files_dest_dir "${CPPDOC_BINARY_DIR}/src/${_target}")

    #[[=
    As of now, sphinx-apidoc can't generate RST files from python stub files.
    See more here: https://github.com/sphinx-doc/sphinx/issues/7630

    You might also wander why we don't use this command directly in
    'add_custom_target'. The problem is that "$1" and "${1%.pyi}" break CMake.
    #=]]
    if("${CMAKE_SYSTEM_NAME}" STREQUAL "Windows")
        message(FATAL_ERROR "cppdoc_add_pyi_files_dir doesn't support Windows platform!")
    else()
        string(CONCAT rename_cmd
            "find \"${py_module_dest_dir}\" -name '*.pyi' -exec "
            "bash -c 'mv -- \"$1\" \"\${1%.pyi}.py\"' bash {} \;"
        )
    endif()

    add_custom_target("${copy_target}"
        COMMAND
            "${CMAKE_COMMAND}" -E copy_directory "${_dir}" "${py_module_dest_dir}"
        COMMAND
            eval "${rename_cmd}"
        COMMENT
            "Copying python stubs of the '${_target}' target to the build directory."
        VERBATIM
    )

    add_custom_target("${gen_target}"
        COMMAND
            sphinx-apidoc -e -f -o "${rst_files_dest_dir}" "${_py_module_name}" *.in
        DEPENDS
            "${copy_target}"
        WORKING_DIRECTORY
            "${CPPDOC_PY_MODULES_BINARY_DIR}"
        COMMENT
            "Generating documentation for python module"
    )

    add_dependencies(cppdoc-copy-rst-files "${gen_target}")
endmacro()

#------------------------------------------------------------------------------#
# Sphinx                                                                       #
#------------------------------------------------------------------------------#

# This is a top-level "meta" target that depends on all other
# 'cppdoc-copy-<target>-rst-files' targets.
add_custom_target(cppdoc-copy-rst-files)

macro(cppdoc_add_rst_files_dir _target _dir)
    set(copy_target "cppdoc-copy-${_target}-rst-files")

    #[[=
    Copy rst files to build directory before generating the html documentation.
    Some of the rst files are generated, so they only exist in the build directory.
    Sphinx needs all files in the same directory in order to generate the html, so
    we need to copy all the non-gnerated rst files from the source to the build
    directory before we run sphinx.
    #=]]

    _cppdoc_get_value(BINARY_DIR)

    add_custom_target("${copy_target}"
        COMMAND
            "${CMAKE_COMMAND}" -E copy_directory "${_dir}" "${CPPDOC_BINARY_DIR}/src/${_target}"
        COMMENT
            "Copying documentation of the '${_target}' target to the build directory."
    )

    add_dependencies(cppdoc-copy-rst-files "${copy_target}")
endmacro()

macro(_cppdoc_add_top_level_rst_files _src _dest)
    set(copy_target "cppdoc-copy-top-level-rst-files")
    add_custom_target("${copy_target}"
        COMMAND
            "${CMAKE_COMMAND}" -E copy_directory "${_src}" "${_dest}"
        COMMENT
            "Copying main documentation to the build directory."
    )

    add_dependencies(cppdoc-copy-rst-files "${copy_target}")
endmacro()

#------------------------------------------------------------------------------#
# Sphinx - Generation                                                          #
#------------------------------------------------------------------------------#

# Explicitly create Doxygen output folder.
file(MAKE_DIRECTORY "${CPPDOC_BINARY_DIR}/doxygen")

set_property(DIRECTORY
    APPEND
        PROPERTY
            # When "clean" target is run,
            ADDITIONAL_MAKE_CLEAN_FILES
                # remove the Doxygen build directory.
                "${CPPDOC_BINARY_DIR}/doxygen"
                # remove the Sphinx build directory.
                "${CPPDOC_BINARY_DIR}/html"
                # remove the doctrees build directory.
                "${CPPDOC_BINARY_DIR}/sphinx/_doctrees"
                # remove the Breathe build directory.
                "${CPPDOC_BINARY_DIR}/sphinx/breathe"
                # remove the src build directory.
                "${CPPDOC_BINARY_DIR}/src"
)

function(_cppdoc_generate_breathe_projects_dependcies)
    get_target_property(CMAKE_PROJECTS cppdoc-meta-info CMAKE_PROJECTS)
    if("${CMAKE_PROJECTS}" STREQUAL "CMAKE_PROJECTS-NOTFOUND")
        message(FATAL_ERROR "Unable to get 'CMAKE_PROJECTS' property from 'cppdoc-meta-info' target.")
    endif()

    foreach (cmake_project IN LISTS CMAKE_PROJECTS)
        _cppdoc_create_breathe_projects_depends_prop("${cmake_project}")
        get_target_property(cmake_project_dependencies "${cmake_project}" "${BREATHE_PROJECTS_DEPENDS_PROP}")

        set(dependencies)
        if(NOT "${cmake_project_dependencies}" STREQUAL "cmake_project_dependencies-NOTFOUND")
            foreach (dependency IN LISTS cmake_project_dependencies)
                get_target_property(original_dependency "${dependency}" ALIASED_TARGET)
                if (original_dependency)
                    list(APPEND dependencies "'${original_dependency}'")
                else()
                    list(APPEND dependencies "'${dependency}'")
                endif()
            endforeach()
        endif()

        list(JOIN dependencies ", " dependencies)
        set_property(
            TARGET
                cppdoc-meta-info
            APPEND
                PROPERTY
                    BREATHE_PROJECTS_DEPENDS "'${cmake_project}': [${dependencies}]"
        )
    endforeach()
endfunction()

function(_cppdoc_generate_conf_py _intput_dir _output_dir)
    # Generate additional content.
    _cppdoc_generate_breathe_projects_dependcies()

    # Breathe
    _cppdoc_get_value(BREATHE_PROJECTS)
    _cppdoc_get_value(BREATHE_PROJECTS_DEPENDS)
    _cppdoc_get_value(BREATHE_PROJECTS_SOURCE)
    # Doxygen
    _cppdoc_get_value(TAGFILE)
    # Exhale
    _cppdoc_get_value(EXHALE_PROJECTS_ARGS)
    # Sphinx
    _cppdoc_get_value(PY_MODULES_BINARY_DIR)
    # Sphinx general configuration
    _cppdoc_get_value(SPHINX_EXTENSIONS ESCAPE)
    # Sphinx options for internationalization
    _cppdoc_get_value(SPHINX_LANGUAGE)
    # Sphinx options for HTML output
    _cppdoc_get_value(SPHINX_FURO_GITLAB_ICONS)
    _cppdoc_get_value(SPHINX_HTML_SHOW_SOURCELINK)
    _cppdoc_get_value(SPHINX_HTML_THEME)
    # Sphinx project info
    _cppdoc_get_value(SPHINX_PROJECT_AUTHOR)
    _cppdoc_get_value(SPHINX_PROJECT_COPYRIGHT)
    _cppdoc_get_value(SPHINX_PROJECT_NAME)
    _cppdoc_get_value(SPHINX_PROJECT_VERSION)

    set(output_file "${_output_dir}/conf.py")

    configure_file("${_intput_dir}/conf.py.in" "${output_file}" @ONLY)

    file(READ "${_intput_dir}/exhale_multiproject_monkeypatch.py" patch)
    file(APPEND "${output_file}" "\n${patch}" )
endfunction()

function(cppdoc_generate_docs)
    set(options ALL)
    set(oneValueArgs)
    set(multiValueArgs)
    cmake_parse_arguments(opts "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if(opts_UNPARSED_ARGUMENTS)
        set(target_name "${opts_UNPARSED_ARGUMENTS}")
    else()
        set(target_name "docs")
    endif()

    if(opts_ALL)
        set(opt_ALL "ALL")
    endif()

    _cppdoc_get_value(BINARY_DIR)
    _cppdoc_get_value(SOURCE_DIR)
    _cppdoc_get_value(CMAKE_MODULES_DIR)
    _cppdoc_get_value(SPHINX_FILES_DIR)

    set(sphinx_dir "${CPPDOC_BINARY_DIR}/sphinx")

    _cppdoc_generate_conf_py("${CPPDOC_SPHINX_FILES_DIR}" "${sphinx_dir}")
    _cppdoc_add_top_level_rst_files("${CPPDOC_SOURCE_DIR}" "${CPPDOC_BINARY_DIR}/src")

    list(APPEND CMAKE_MODULE_PATH "${CPPDOC_CMAKE_MODULES_DIR}")
    find_package(Sphinx REQUIRED breathe exhale)

    add_custom_target("${target_name}"
        ${opt_ALL}
        COMMAND
            ${SPHINX_BUILD_EXECUTABLE}
                -b html
                -c "${sphinx_dir}"
                -d "${sphinx_dir}/_doctrees"
                -j auto
                "${CPPDOC_BINARY_DIR}/src"
                "${CPPDOC_BINARY_DIR}/html"
        DEPENDS
            cppdoc-copy-rst-files
        VERBATIM
        WORKING_DIRECTORY
            "${sphinx_dir}"
        COMMENT
            "Generating documentation with Sphinx"
    )
endfunction()
