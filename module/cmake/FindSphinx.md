# Sphinx integration for CMake

A CMake module for Sphinx inspired by the excellent Doxygen module.
Given that you have CMake, Doxygen, Sphinx (+Breathe) installed, generating API documentation can be done by:

# Acknowledgment

Based on [k0ekk0ek/cmake-sphinx](https://github.com/k0ekk0ek/cmake-sphinx) work.
