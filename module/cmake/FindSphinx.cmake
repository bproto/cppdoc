include(FindPackageHandleStandardArgs)

macro(_Sphinx_find_executable _exe)
  string(TOUPPER "${_exe}" _uc)
  # sphinx-(build|quickstart)-3 x.x.x
  # FIXME: This works on Fedora (and probably most other UNIX like targets).
  #        Windows targets and PIP installs might need some work.
  find_program(
    SPHINX_${_uc}_EXECUTABLE
    NAMES "sphinx-${_exe}-3" "sphinx-${_exe}" "sphinx-${_exe}.exe")

  if(SPHINX_${_uc}_EXECUTABLE)
    execute_process(
      COMMAND "${SPHINX_${_uc}_EXECUTABLE}" --version
      RESULT_VARIABLE _result
      OUTPUT_VARIABLE _output
      OUTPUT_STRIP_TRAILING_WHITESPACE)
    if(_result EQUAL 0 AND _output MATCHES " v?([0-9]+\\.[0-9]+\\.[0-9]+)$")
      set(SPHINX_${_uc}_VERSION "${CMAKE_MATCH_1}")
    endif()

    if(NOT TARGET Sphinx::${_exe})
      add_executable(Sphinx::${_exe} IMPORTED GLOBAL)
      set_target_properties(Sphinx::${_exe} PROPERTIES
        IMPORTED_LOCATION "${SPHINX_${_uc}_EXECUTABLE}")
    endif()
    set(Sphinx_${_exe}_FOUND TRUE)
  else()
    set(Sphinx_${_exe}_FOUND FALSE)
  endif()
  unset(_uc)
endmacro()

macro(_Sphinx_find_module _name _module)
  string(TOUPPER "${_name}" _Sphinx_uc)
  if(SPHINX_PYTHON_EXECUTABLE)
    execute_process(
      COMMAND ${SPHINX_PYTHON_EXECUTABLE} -m ${_module} --version
      RESULT_VARIABLE _result
      OUTPUT_VARIABLE _output
      OUTPUT_STRIP_TRAILING_WHITESPACE
      ERROR_QUIET)
    if(_result EQUAL 0)
      if(_output MATCHES " v?([0-9]+\\.[0-9]+\\.[0-9]+)$")
        set(SPHINX_${_Sphinx_uc}_VERSION "${CMAKE_MATCH_1}")
      endif()

      if(NOT TARGET Sphinx::${_name})
        set(SPHINX_${_Sphinx_uc}_EXECUTABLE "${SPHINX_PYTHON_EXECUTABLE} -m ${_module}")
        add_executable(Sphinx::${_name} IMPORTED GLOBAL)
        set_target_properties(Sphinx::${_name} PROPERTIES
          IMPORTED_LOCATION "${SPHINX_PYTHON_EXECUTABLE}")
      endif()
      set(Sphinx_${_name}_ARGS -m ${_module})
      set(Sphinx_${_name}_FOUND TRUE)
    else()
      set(Sphinx_${_name}_FOUND FALSE)
    endif()
  else()
    set(Sphinx_${_name}_FOUND FALSE)
  endif()
  unset(_Sphinx_uc)
endmacro()

macro(_Sphinx_find_extension _ext)
  if(SPHINX_PYTHON_EXECUTABLE)
    execute_process(
      COMMAND ${SPHINX_PYTHON_EXECUTABLE} -c "import ${_ext}"
      RESULT_VARIABLE _result)
    if(_result EQUAL 0)
      set(Sphinx_${_ext}_FOUND TRUE)
    else()
      set(Sphinx_${_ext}_FOUND FALSE)
    endif()
  endif()
endmacro()

#
# Find sphinx-build and sphinx-quickstart.
#

# Find sphinx-build shim.
_Sphinx_find_executable(build)

if(SPHINX_BUILD_EXECUTABLE)
  # Find sphinx-quickstart shim.
  _Sphinx_find_executable(quickstart)

  # Locate Python executable
  if(CMAKE_HOST_WIN32)
    # script-build on Windows located under (when PIP is used):
    # C:/Program Files/PythonXX/Scripts
    # C:/Users/username/AppData/Roaming/Python/PythonXX/Sripts
    #
    # Python modules are installed under:
    # C:/Program Files/PythonXX/Lib
    # C:/Users/username/AppData/Roaming/Python/PythonXX/site-packages
    #
    # To verify a given module is installed, use the Python base directory
    # and test if either Lib/module.py or site-packages/module.py exists.
    get_filename_component(_Sphinx_directory "${SPHINX_BUILD_EXECUTABLE}" DIRECTORY)
    get_filename_component(_Sphinx_directory "${_Sphinx_directory}" DIRECTORY)
    if(EXISTS "${_Sphinx_directory}/python.exe")
      set(SPHINX_PYTHON_EXECUTABLE "${_Sphinx_directory}/python.exe")
    endif()
    unset(_Sphinx_directory)
  else()
    file(READ "${SPHINX_BUILD_EXECUTABLE}" _Sphinx_script)
    if(_Sphinx_script MATCHES "^#!([^\n]+)")
      string(STRIP "${CMAKE_MATCH_1}" _Sphinx_shebang)
      if(EXISTS "${_Sphinx_shebang}")
        set(SPHINX_PYTHON_EXECUTABLE "${_Sphinx_shebang}")
      endif()
    endif()
    unset(_Sphinx_script)
    unset(_Sphinx_shebang)
  endif()
endif()

if(NOT SPHINX_PYTHON_EXECUTABLE)
  # Python executable cannot be extracted from shim shebang or path if e.g.
  # virtual environments are used, fallback to find package. Assume the
  # correct installation is found, the setup is probably broken in more ways
  # than one otherwise.
  find_package(Python3 QUIET COMPONENTS Interpreter)
  if(TARGET Python3::Interpreter)
    set(SPHINX_PYTHON_EXECUTABLE ${Python3_EXECUTABLE})
    # Revert to "python -m sphinx" if shim cannot be found.
    if(NOT SPHINX_BUILD_EXECUTABLE)
      _Sphinx_find_module(build sphinx)
      _Sphinx_find_module(quickstart sphinx.cmd.quickstart)
    endif()
  endif()
endif()

#
# Verify components are available.
#
if(SPHINX_BUILD_VERSION)
  # Breathe is required for Exhale
  if("exhale"  IN_LIST Sphinx_FIND_COMPONENTS AND NOT
     "breathe" IN_LIST Sphinx_FIND_COMPONENTS)
    list(APPEND Sphinx_FIND_COMPONENTS "breathe")
  endif()

  foreach(_Sphinx_component IN LISTS Sphinx_FIND_COMPONENTS)
    if(_Sphinx_component STREQUAL "build")
      # Do nothing, sphinx-build is always required.
      continue()
    elseif(_Sphinx_component STREQUAL "quickstart")
      # Do nothing, sphinx-quickstart is optional, but looked up by default.
      continue()
    endif()
    _Sphinx_find_extension(${_Sphinx_component})
  endforeach()
  unset(_Sphinx_component)

  #
  # Verify both executables are part of the Sphinx distribution.
  #
  if(SPHINX_QUICKSTART_VERSION AND NOT SPHINX_BUILD_VERSION STREQUAL SPHINX_QUICKSTART_VERSION)
    message(FATAL_ERROR "Versions for sphinx-build (${SPHINX_BUILD_VERSION}) "
                        "and sphinx-quickstart (${SPHINX_QUICKSTART_VERSION}) "
                        "do not match")
  endif()
endif()

find_package_handle_standard_args(
  Sphinx
  VERSION_VAR SPHINX_BUILD_VERSION
  REQUIRED_VARS SPHINX_BUILD_EXECUTABLE SPHINX_BUILD_VERSION
  HANDLE_COMPONENTS)
