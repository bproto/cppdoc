# -*- coding: utf-8 -*-

# Copyright 2021-2024 Bytes Mess <b110011@pm.me>.
#
# CppDoc: CMake Module for an easy generation of C++ documentation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Monkey patch exhale to support multiple doxygen projects at one time.
# Based on https://github.com/mithro/sphinx-contrib-mithro/tree/master/sphinx-contrib-exhale-multiproject
# Because of https://github.com/svenevs/exhale/issues/27

import os
import os.path
import types
from pprint import pprint

import exhale
import exhale.configs
import exhale.deploy
import exhale.graph
import exhale.utils


class MonkeyPatch:
    def __init__(self) -> None:
        self._current_project = None
        self._node_by_refid = {}
        self._debug = os.environ.get("ENABLE_EXHALE_DEBUG_OUTPUT", False)

    # Patched methods

    def explode(self):
        # Quick sanity check to make sure the bare minimum have been set in the configs
        err_msg = (
            "`configs.{config}` was `None`.  Do not call `deploy.explode` directly."
        )
        if exhale.configs.containmentFolder is None:
            raise RuntimeError(err_msg.format(config="containmentFolder"))
        if exhale.configs.rootFileName is None:
            raise RuntimeError(err_msg.format(config="rootFileName"))
        if exhale.configs.doxygenStripFromPath is None:
            raise RuntimeError(err_msg.format(config="doxygenStripFromPath"))

        # From here on, we assume that everything else has been checked / configured.
        try:
            textRoot = exhale.deploy.ExhaleRoot()
            for k, v in self._node_by_refid.items():
                textRoot.node_by_refid[k] = v
        except:
            exhale.utils.fancyError("Unable to create an `ExhaleRoot` object:")

        try:
            os.sys.stdout.write(
                "{0}\n".format(exhale.utils.info("Exhale: parsing Doxygen XML."))
            )
            start = exhale.utils.get_time()

            textRoot.parse()

            end = exhale.utils.get_time()
            os.sys.stdout.write(
                "{0}\n".format(
                    exhale.utils.progress(
                        "Exhale: finished parsing Doxygen XML in {0}.".format(
                            exhale.utils.time_string(start, end)
                        )
                    )
                )
            )
        except:
            exhale.utils.fancyError("Exception caught while parsing:")
        try:
            os.sys.stdout.write(
                "{0}\n".format(
                    exhale.utils.info("Exhale: generating reStructuredText documents.")
                )
            )
            start = exhale.utils.get_time()

            textRoot.generateFullAPI()

            end = exhale.utils.get_time()
            os.sys.stdout.write(
                "{0}\n".format(
                    exhale.utils.progress(
                        "Exhale: generated reStructuredText documents in {0}.".format(
                            exhale.utils.time_string(start, end)
                        )
                    )
                )
            )
        except:
            exhale.utils.fancyError("Exception caught while generating:")

        # << verboseBuild
        #   toConsole only prints if verbose mode is enabled
        textRoot.toConsole()

        # allow access to the result after-the-fact
        exhale.configs._the_app.exhale_root = textRoot

        self._node_by_refid = {**self._node_by_refid, **textRoot.node_by_refid}

    def exhaleEnvironmentReady(self, app) -> None:
        self._current_project = app.config.breathe_default_project
        default_project = self._current_project

        default_exhale_args = dict(app.config.exhale_args)

        exhale_projects_args = dict(app.config._raw_config["exhale_projects_args"])
        breathe_projects = dict(app.config._raw_config["breathe_projects"])
        breathe_projects_depends = dict(
            app.config._raw_config["breathe_projects_depends"]
        )

        seen_projects = []
        for project in breathe_projects:
            depends = breathe_projects_depends.get(project, [])
            depends.append(project)
            for dep in depends:
                if dep in seen_projects:
                    continue

                seen_projects.append(dep)

                self._current_project = dep
                app.config.breathe_default_project = dep

                project_exhale_args = exhale_projects_args.get(dep, {})

                app.config.exhale_args = dict(default_exhale_args)
                app.config.exhale_args.update(project_exhale_args)
                app.config.exhale_args["containmentFolder"] = os.path.realpath(
                    app.config.exhale_args["containmentFolder"]
                )

                if self._debug:
                    print("=" * 75)
                    print(dep)
                    print("-" * 50)
                    pprint(app.config.exhale_args)
                    print("=" * 75)

                # First, setup the extension and verify all of the configurations.
                exhale.configs.apply_sphinx_configurations(app)
                ####### Next, perform any cleanup

                # Generate the full API!
                try:
                    self.explode()
                except:
                    exhale.utils.fancyError(
                        "Exhale: could not generate reStructuredText documents :/"
                    )

        app.config.breathe_default_project = default_project

    def makeCustomSpecificationsMapping(self, func):
        # Make sure they gave us a function
        if not isinstance(func, types.FunctionType):
            raise ValueError(
                f"The input to exhale.util.makeCustomSpecificationsMapping was *NOT* a function: {type(func)}"
            )

        return exhale.utils.makeCustomSpecificationsMapping(self._patchSpec(func))

    def specificationsForKind(self):
        return self._patchSpec(exhale.utils.specificationsForKind)

    # Utility methods

    def _patchSpec(self, func):
        def wrap(kind):
            spec = func(kind)
            spec.insert(0, f":project: {self._current_project}")
            return spec

        return wrap


def exhaleEnvironmentReady(app):
    mp = MonkeyPatch()

    exhale.utils.makeCustomSpecificationsMapping = (
        lambda func: mp.makeCustomSpecificationsMapping(func)
    )

    exhale.utils.specificationsForKind = mp.specificationsForKind()

    mp.exhaleEnvironmentReady(app)


exhale.environment_ready = exhaleEnvironmentReady
