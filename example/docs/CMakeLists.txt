# Copyright 2022-2024 Bytes Mess <b110011@pm.me>
#
# CppDoc: Example project.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#-----------------------------------------------------------------------------#
# Configuration setup                                                         #
#-----------------------------------------------------------------------------#

set(CPPDOC_BINARY_DIR "${CMAKE_CURRENT_BINARY_DIR}")
set(CPPDOC_SOURCE_DIR "${CMAKE_CURRENT_BINARY_DIR}")

# Sphinx general configuration

set(CPPDOC_SPHINX_EXTENSIONS
    # Built-in
    "sphinx.ext.autodoc"
)

# Sphinx options for HTML output

set(SPHINX_FURO_GITLAB_ICONS ON)
set(CPPDOC_SPHINX_HTML_THEME "furo")

# Sphinx project info

set(CPPDOC_SPHINX_PROJECT_COPYRIGHT "2022-{}, John Doe")
set(CPPDOC_SPHINX_PROJECT_NAME "CppDoc: Example")

#-----------------------------------------------------------------------------#
# Package setup                                                               #
#-----------------------------------------------------------------------------#

if(CPPDOC_EXAMPLE)
    include(../../module/Index.cmake)
else()
    find_package(cppdoc REQUIRED NO_MODULE)
endif()
