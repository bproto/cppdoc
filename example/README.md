# CppDoc: Example

This is a fake project that is used to test `cppdoc`.

**Table of contents** (generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/))

- [Build](#build)
  - ["Local"](#local)
  - ["System"](#system)
- [Folder Structure](#folder-structure)
- [License](#license)

## Build

### "Local"

You can generate documentation for an "example" project using a top-level CMake project, which is located in the root of the repo.

In order to do so, you need to run following commands:

```bash
# pwd: root of the repo.
$ cmake -S. -Bbuild -DCPPDOC_ENABLE=ON -DCPPDOC_EXAMPLE=ON
$ cmake --build build
# or using a target:
$ cmake --build build --target cppdoc-example-docs
```

Generated HTML documentation can be found by the following path:

- `build/example/docs/html`.

### "System"

You can generate documentation using only "example" project to emulate "real"-like usage.

In order to do so, first you need to install `cppdoc` system-wide by running following commands:

```bash
# pwd: root of the repo.
$ cmake -S. -Bbuild
$ sudo cmake --build build --target install
```

Then you need to generate HTML documentation using only an "example project":

```bash
# pwd: root of the repo.
$ cmake -Sexample -Bbuild-example -DCPPDOC_ENABLE=ON
$ cmake --build ./build-example
# or using a target:
$ cmake --build build-example --target cppdoc-example-docs
```

Generated HTML documentation can be found by the following path:

- `build-example/docs/html`.

## Folder Structure

- `doc`: an example of top-level documentation.
- `project/**/docs`: an example of module-level documentation.
- `project/**/include`: an example of documentation that usage `doxygen` annotated files.
- `project/bindings/python`: an example of documentation that usage python stabs.

## License

This library is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).
