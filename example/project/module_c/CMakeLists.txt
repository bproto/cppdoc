# Copyright 2022-2024 Bytes Mess <b110011@pm.me>
#
# CppDoc: Example project.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(cppdoc-example-module-c
    LANGUAGES
        CXX
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(API_HEADERS
    accessor.hpp
)

list(TRANSFORM API_HEADERS PREPEND "include/module_c/")
set(API_HEADERS_DIR ${CMAKE_CURRENT_SOURCE_DIR}/include)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" INTERFACE)
add_library("CppDoc::Example::ModuleC" ALIAS "${PROJECT_NAME}")

cppdoc_add_cpp_files_dir("${PROJECT_NAME}"
    SOURCES
        "${API_HEADERS_DIR}"
    DEPENDS
        CppDoc::Example::ModuleB
)
cppdoc_add_rst_files_dir("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

source_group("api" FILES ${API_HEADERS})

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
    INTERFACE
        $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
        $<INSTALL_INTERFACE:include>
)

#------------------------------------------------------------------------------#
# Project libraries                                                            #
#------------------------------------------------------------------------------#

target_link_libraries("${PROJECT_NAME}"
    INTERFACE
        CppDoc::Example::ModuleB
)
