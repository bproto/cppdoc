Python
======

This is an example of C++ module which provides ``Python`` bindings in a form of a `python native extension <https://docs.python.org/3/extending/index.html>`__.

.. toctree::
   :hidden:
   :maxdepth: 1

   developer_notes.rst
   package.rst
