################################################################
CppDoc: CMake Module for an easy generation of C++ documentation
################################################################

.. toctree::
   :caption: Package
   :hidden:
   :maxdepth: 1

   module/index.rst
   find-module/index.rst

   Example <https://bproto.gitlab.io/cppdoc/example>

.. toctree::
   :caption: Development
   :hidden:
   :maxdepth: 1

   git/index.rst

.. _doxygen: https://www.doxygen.nl
.. _sphinx: https://www.sphinx-doc.org
.. _breathe: https://github.com/breathe-doc/breathe
.. _exhale: https://github.com/svenevs/exhale

`cppdoc` provides a `cmake` module that makes it easier to generate HTML documentation with a help of `doxygen`_ and `sphinx`_ packages, as well as `breathe`_ and `exhale`_ ``sphinx`` extensions.

Install
=======

Prerequisites
-------------

System packages
^^^^^^^^^^^^^^^

The only system prerequisite of ``cppdoc`` is ``cmake``, ``doxygen`` and ``python3-pip`` packages.

Ubuntu users can install those packages with the official package manager:

.. prompt:: bash

   sudo apt install cmake doxygen python3-pip

Python packages
^^^^^^^^^^^^^^^

The only system prerequisite of ``cppdoc`` is ``sphinx``, ``breathe`` and ``exhale`` packages.

Ubuntu users can install those packages with the ``pip`` package manager:

.. prompt:: bash

   pip install sphinx breathe exhale

However for time been it's recommended to install those python packages with versions specified in ``requirements.txt`` file:

.. prompt:: bash

   pip install -r requirements.txt

Building from source
--------------------

To build and install from source, you can clone the repo and use cmake to install the modules as though this is a regular cmake project:

.. prompt:: bash

   git clone https://gitlab.com/bproto/cppdoc.git
   cd cppdoc
   # local install:
   cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=/path/to/install/dir
   cmake --build build --target install
   # system wide install:
   cmake -S. -Bbuild
   sudo cmake --build build --target install

Replace ``/path/to/install/dir`` to whatever directory you want to install this package to.

Usage
=====

To use ``cppdoc``, add the following to your ``CMakeLists.txt``:

.. code-block:: cmake

   set(CPPDOC_BINARY_DIR "${CMAKE_BINARY_DIR}/docs")
   set(CPPDOC_SOURCE_DIR "${CMAKE_SOURCE_DIR}/docs")

   find_package(cppdoc REQUIRED NO_MODULE)

You can then just use methods provided by ``cppdoc`` as needed.
For example:

.. code-block:: cmake

   # Add doxygen annotated files.
   cppdoc_add_cpp_files_dir("${PROJECT_NAME}"
     SOURCES
       ...
     DEPENDS
       ...
   )

   # Add RST files.
   cppdoc_add_rst_files_dir("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

   # Generate HTML docs
   cppdoc_generate_docs()

License
=======

All code is licensed under the `Apache 2.0 <https://www.apache.org/licenses/LICENSE-2.0>`__ license.
