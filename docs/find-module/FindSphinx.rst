FindSphinx
----------

.. note::
    Based on `k0ekk0ek/cmake-sphinx <https://github.com/k0ekk0ek/cmake-sphinx>`__ work.

Sphinx is a documentation generation tool (see https://www.sphinx-doc.org).
This module looks for Sphinx and some optional tools it supports:

``breathe``
    `Breathe <https://github.com/breathe-doc/breathe>`__ a Sphinx plugin which provides integration of Doxygen output into RST documentation.

``exhale``
  `Exhale <https://github.com/svenevs/exhale>`__ a Sphinx plugin which automatically generate of Sphinx sources that, using the ``breathe`` extension, document a whole package in the style of other automatic API documentation tools.

.. code-block:: cmake

  find_package(Sphinx
               REQUIRED breathe exhale)

The following variables are defined by this module:

**SPHINX_FOUND**:BOOLEAN
    True if the ``sphinx`` executable was found.

**SPHINX_BUILD_EXECUTABLE**:PATH
    Absolute path of the found ``sphinx-build`` tool.

**SPHINX_BUILD_VERSION**:VERSION
    The version reported by ``sphinx-build --version``.

**SPHINX_PYTHON_EXECUTABLE**:PATH
    Python executable used to run ``sphinx-build``.

The module defines ``IMPORTED`` target for ``sphinx-build``.
These can be used as part of custom commands, etc. and should be preferred over old-style (and now deprecated) ``SPHINX_BUILD_EXECUTABLE`` variable.
The following import target is defined if ``sphinx-build`` executable could be found:

::

  Sphinx::build
