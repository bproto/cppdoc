###########################
FindModule(s) documentation
###########################

.. toctree::
   :hidden:
   :maxdepth: 1

   FindSphinx.rst

Introduction
============

Find modules are used by the CMake ``find_package`` command to search for packages that do not provide their own CMake package config files.

``cppdoc`` internally uses some custom "find modules" which are listed on this page.

All Find Modules
================

:doc:`FindSphinx`
    A "find module" for a `sphinx <https://www.sphinx-doc.org>`__ package, developed by `@k0ekk0ek <https://github.com/k0ekk0ek/cmake-sphinx>`__.
