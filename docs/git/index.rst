##########
Git Guides
##########

These documents cover GIT-related stuff that is specific for this project.

.. toctree::
   :maxdepth: 1
   :hidden:

   tags.rst

:doc:`tags`
    A list of all available tags for commit messages.
