===================
Commit Message Tags
===================

A list of all available tags for commit messages.

Meta Tags
=========

Here is collected a full list of all available *meta* tags for commit messages.

Please take into account that these tags unlike others have a generalized meaning.
That why it's hard to collect all possible cases when a particular tag should be used.

.. table::
    :widths: 60 10 60

    +---------------------------+--------+----------------------------------------+
    | Name                      | Active | Pattern                                |
    +===========================+========+========================================+
    | CI                        | Yes    | ``.gitlab/ci/*``                       |
    +---------------------------+--------+----------------------------------------+
    | CMake                     | Yes    | ``**/conan.*``                         |
    +---------------------------+--------+----------------------------------------+
    | Docs                      | Yes    | ``docs/*``                             |
    +---------------------------+--------+----------------------------------------+
    | Example                   | Yes    | ``example/*``                          |
    +---------------------------+--------+----------------------------------------+

