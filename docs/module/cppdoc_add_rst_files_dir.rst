.. _cppdoc_add_rst_files_dir:

########################
cppdoc_add_rst_files_dir
########################

Mark a directory with RST files, which belongs to specified target, as once that should take part in HTML documentation generation.

.. code-block:: cmake

   cppdoc_add_rst_files_dir(<target> <rstDocsDirectory>)

This function is intended as a convenience for generating documentation from RST files for a specific target.

.. warning::
   Per current implementation can be called more than once per target.
   However sequential calls might produce unexpected results.

Arguments
---------

.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_library`_ and must not be an `ALIAS`_ target.

The ``<rstDocsDirectory>`` argument must be a valid path to a directory with RST files.

Output
------

``cppdoc_add_rst_files_dir`` will copy RST files that reside by ``<rstDocsDirectory>`` path to following directory:

- ``<CPPDOC_BINARY_DIR>/src/<target>``.
