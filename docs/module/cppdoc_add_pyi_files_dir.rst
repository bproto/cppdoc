.. _cppdoc_add_pyi_files_dir:

########################
cppdoc_add_pyi_files_dir
########################

Mark a directory with python stub (*pyi*) files, which belongs to specified target, as once that should take part in HTML documentation generation.

.. code-block:: cmake

   cppdoc_add_pyi_files_dir(<pyModuleName> <target> <pyiStubsDirectory>)

This function is intended as a convenience for generating documentation from python stub files for a specific target.
It aims to automate and simplify work with ``sphinx-apidoc`` and python stub files.

.. warning::
   Per current implementation can be called more than once per target.
   However sequential calls might produce unexpected results.

.. warning::
   Per current implementation this function is available only on Linux systems.

Arguments
---------

.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The ``<pyModuleName>`` argument must be a .

The named ``<target>`` must have been created by a command such as `add_library`_ and must not be an `ALIAS`_ target.

The ``<pyiStubsDirectory>`` argument must be a valid path to a directory with python stub files.

Requirements
------------

.. note:: This method requires `sphinx.ext.autodoc <https://www.sphinx-doc.org/en/master/usage/extensions/autodoc.html>`__ extension to be enabled in your `conf.py`.

Output
------

``cppdoc_add_pyi_files_dir`` will copy python stub files that reside by ``<pyiStubsDirectory>`` path to the following directory:

- ``<CPPDOC_PY_MODULES_BINARY_DIR>/<pyModuleName>``.

``sphinx-apidoc`` generated RST files will reside by this the following path:

- ``${CPPDOC_BINARY_DIR}/src/<target>``
