####################
Module documentation
####################

These documents cover in-detail all public functions and configuration options of ``cppdoc`` package.

.. toctree::
   :hidden:
   :maxdepth: 1

   options.rst
   diagrams.rst
   cppdoc_add_cpp_files_dir.rst
   cppdoc_add_pyi_files_dir.rst
   cppdoc_add_rst_files_dir.rst
   cppdoc_generate_docs.rst

:doc:`options`
    A list of all available configuration options of ``cppdoc`` package.

:doc:`diagrams`
    A set of useful diagrams that can help to understand how things work.

A list of all public functions of ``cppdoc`` package:

- :doc:`cppdoc_add_cpp_files_dir`,
- :doc:`cppdoc_add_pyi_files_dir`,
- :doc:`cppdoc_add_rst_files_dir`,
- :doc:`cppdoc_generate_docs`.
