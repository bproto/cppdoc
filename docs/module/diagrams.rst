##############
Fancy diagrams
##############

Dependency graph for custom targets
===================================

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/custom_targets_dep_graph.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_static/custom_targets_dep_graph.png
   :target: ../_images/custom_targets_dep_graph.png
   :alt: Dependency graph for custom targets
   :class: only-light

.. image:: /_static/custom_targets_dep_graph.dark.png
   :target: ../_images/custom_targets_dep_graph.dark.png
   :alt: Dependency graph for custom targets
   :class: only-dark

Action flow chart
=================

Configuration stage
-------------------

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/conf_stage_actions.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_static/conf_stage_actions.png
   :target: ../_images/conf_stage_actions.png
   :alt: Dependency graph for custom targets
   :align: center
   :class: only-light

.. image:: /_static/conf_stage_actions.dark.png
   :target: ../_images/conf_stage_actions.dark.png
   :alt: Dependency graph for custom targets
   :align: center
   :class: only-dark

Build stage
-----------

..
  Flowchart has been generated with the help of https://app.diagrams.net/.
  If you need to change flowchart:
    1. Go to the mentioned site.
    2. Import _static/build_stage_actions.drawio.
    3. Change flowchart
    4. Save as .drawio and .png files.
    5. Update appropriate files in _static folder.

.. image:: /_static/build_stage_actions.png
   :target: ../_images/build_stage_actions.png
   :alt: Dependency graph for custom targets
   :class: only-light

.. image:: /_static/build_stage_actions.dark.png
   :target: ../_images/build_stage_actions.dark.png
   :alt: Dependency graph for custom targets
   :class: only-dark

