.. _cppdoc_add_cpp_files_dir:

########################
cppdoc_add_cpp_files_dir
########################

Mark a directory with ``doxygen`` annotated C++ files, which belongs to specified target, as once that should take part in HTML documentation generation.

.. code-block:: cmake

   cppdoc_add_cpp_files_dir(<target>
                            SOURCES <dir>
                            [DEPENDS <dependency>...])

.. _breathe: https://github.com/breathe-doc/breathe
.. _doxygen: https://www.doxygen.nl
.. _exhale: https://github.com/svenevs/exhale

This function is intended as a convenience for generating API documentation for ``doxygen`` annotated files of a specific target.
It aims to automate and simplify work with `doxygen`_, `breathe`_ and `exhale`_ packages.

.. warning:: Per current implementation limitations this function must be called only once per target.

Arguments
---------

.. _add_library: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _ALIAS: https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#alias-targets

The named ``<target>`` must have been created by a command such as `add_library`_ and must not be an `ALIAS`_ target.

The ``SOURCES`` keyword is required to specify a directory which should be automatically processed with ``doxygen``, ``breathe`` and ``exhale`` packages.

The ``DEPENDS`` keyword can be used to specify dependencies for target documentation.

Each ``<dependency>...`` must meet following requirements:

- can be a library target name, which is created by `add_library`_ within the project;
- can be a `ALIAS`_ library target name;
- can be defined later in CMake files but before :ref:`cppdoc_generate_docs`;
- must be marked with ``cppdoc_add_cpp_files_dir``.

If annotated files `reference <https://www.doxygen.nl/manual/commands.html#cmdref>`__ to entities defined by other CMake submodules, it is recommenced to specify those submodules via ``DEPENDS`` keyword to get correct links in generated documentation.

Linting
-------

If ``clang`` is detected as a main compiler, ``cppdoc_add_cpp_files_dir`` will enable ``clang`` specific checks related to documentation comments.

This includes checking that ``\param`` commands name parameters that actually present in the function signature, checking that ``\returns`` is used only on functions that actually return a value etc.

**See also:** `-Wdocumentation <https://clang.llvm.org/docs/UsersManual.html#cmdoption-wdocumentation>`__ option.

Requirements
------------

.. note:: This method requires `doxygen`_ package to be installed on your OS.

.. note:: This method requires `breathe`_ and `exhale`_ extensions to be enabled in your `conf.py`.

Output
------

``doxygen`` tag file will reside by this the following path:

- ``<CPPDOC_BINARY_DIR>/doxygen/<target>/xml``.

``exhale`` generated RST files will reside by this the following path:

- ``<CPPDOC_BINARY_DIR>/src/<target>``.
