.. _cppdoc_generate_docs:

####################
cppdoc_generate_docs
####################

Generate HTML documentation from files that reside in directories specified by :ref:`cppdoc_add_cpp_files_dir`, :ref:`cppdoc_add_pyi_files_dir` and :ref:`cppdoc_add_rst_files_dir` functions.

.. code-block:: cmake

   cppdoc_generate_docs(targetName [ALL])

.. _doxygen: https://www.doxygen.nl
.. _sphinx: https://www.sphinx-doc.org
.. _breathe: https://github.com/breathe-doc/breathe
.. _exhale: https://github.com/svenevs/exhale

This function is intended as a convenience for adding a target for generating documentation with a help of `doxygen`_ and `sphinx`_ packages, as well as `breathe`_ and `exhale`_ ``sphinx`` extensions.
It aims to provide sensible defaults so that projects can generally just provide the input files and directories and that will be sufficient to give sensible results.
The function supports the ability to customize the Sphinx configuration used to build the documentation.

The function constructs a ``conf.py`` and defines a custom target that runs Sphinx on that generated file.

The contents of the generated ``conf.py`` can be customized to some extent by setting CMake variables before calling ``find_package(~)``.

Some of Sphinx's configuration options are overridden to provide more appropriate behavior for a CMake project.
Each of the following will be explicitly set unless the variable already has a value before ``find_package(~)`` is called:

- :ref:`CPPDOC_SPHINX_EXTENSIONS <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_HTML_SHOW_SOURCELINK <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_HTML_THEME <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_LANGUAGE <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_PROJECT_AUTHOR <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_PROJECT_COPYRIGHT <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_PROJECT_NAME <cppdoc-options>`,
- :ref:`CPPDOC_SPHINX_PROJECT_VERSION <cppdoc-options>`.

To change any of above options, set relevant variables before calling ``find_package(~)``.
For example:

.. code-block:: cmake

   set(CPPDOC_BINARY_DIR "${CMAKE_BINARY_DIR}/docs")
   set(CPPDOC_SOURCE_DIR "${CMAKE_SOURCE_DIR}/docs")

   # Change default values:
   set(CPPDOC_SPHINX_EXTENSIONS "sphinx.ext.graphviz")
   set(CPPDOC_SPHINX_HTML_THEME "furo")
   set(CPPDOC_SPHINX_PROJECT_COPYRIGHT "1990-{}, John Doe")
   set(CPPDOC_SPHINX_PROJECT_NAME "My awesome C++ project")

   find_package(cppdoc REQUIRED NO_MODULE)

   # Add directories...

   cppdoc_generate_docs()

.. warning::
   Per current implementation must be called only once.

Arguments
---------

.. _add_custom_target: https://cmake.org/cmake/help/latest/command/add_custom_target.html

If ``targetName`` provided, it will be passed as the name for the `add_custom_target`_ command instead of default ``docs`` value.

If ``ALL`` is set, the target will be added to the default build target.

Output
------

Generated ``conf.py`` configuration file for ``sphinx`` package will reside by this the following path:

- ``<CPPDOC_BINARY_DIR>/sphinx/conf.py``.

Generated RST files will reside by this the following path:

- ``<CPPDOC_BINARY_DIR>/html``.
