# CppDoc: CMake Module for an easy generation of C++ documentation

`cppdoc` provides a `cmake` module that makes it easier to generate HTML documentation with a help of [doxygen][1] and [sphinx][2] packages, as well as [breathe][3] and [exhale][4] `sphinx` extensions.

[1]: https://www.doxygen.nl
[2]: https://www.sphinx-doc.org
[3]: https://github.com/breathe-doc/breathe
[4]: https://github.com/svenevs/exhale

**Table of contents** (generated with [markdown-toc](http://ecotrust-canada.github.io/markdown-toc/))

- [Install](#install)
  - [Prerequisites](#prerequisites)
    - [System packages](#system-packages)
    - [Python packages](#python-packages)
  - [Building from source](#building-from-source)
- [Usage](#usage)
- [Folder Structure](#folder-structure)
- [License](#license)

## Install

### Prerequisites

#### System packages

The only system prerequisite of `cppdoc` is `cmake`, `doxygen` and `python3-pip` packages.

Ubuntu users can install those packages with the official package manager:

```bash
$ sudo apt install cmake doxygen python3-pip
```

#### Python packages

The only python prerequisite of `cppdoc` is `sphinx`, `breathe` and `exhale` packages.

Ubuntu users can install those packages with the `pip` package manager:

```bash
$ pip install sphinx breathe exhale
```

However for time been it's recommended to install those python packages with versions specified in `requirements.txt` file:

```bash
$ pip install -r requirements.txt
```

### Building from source

To build and install from source, you can clone the repo and use cmake to install the modules as though this is a regular cmake project:

```bash
$ git clone https://gitlab.com/bproto/cppdoc.git
$ cd cppdoc
# local install:
$ cmake -S. -Bbuild -DCMAKE_INSTALL_PREFIX=/path/to/install/dir
$ cmake --build build --target install
# system wide install:
$ cmake -S. -Bbuild
$ sudo cmake --build build --target install
```

Replace `/path/to/install/dir` to whatever directory you want to install this package to.

## Usage

To use `cppdoc`, add the following to your `CMakeLists.txt`:

```cmake
set(CPPDOC_BINARY_DIR "${CMAKE_BINARY_DIR}/docs")
set(CPPDOC_SOURCE_DIR "${CMAKE_SOURCE_DIR}/docs")

find_package(cppdoc REQUIRED NO_MODULE)
```

You can then just use methods provided by `cppdoc` as needed.
For example:

```cmake
# Add doxygen annotated files.
cppdoc_add_cpp_files_dir("${PROJECT_NAME}"
  SOURCES
    ...
  DEPENDS
    ...
)

# Add RST files.
cppdoc_add_rst_files_dir("${PROJECT_NAME}" "${CMAKE_CURRENT_SOURCE_DIR}/docs")

# Generate HTML docs
cppdoc_generate_docs()
```

## Folder Structure

- `docs`: a comprehensive project documentation.
- `example`: a fake project that is used to test `cppdoc`.
- `module`: cmake module that get installed by this package.
- `package`: template files for producing the config-files of `cppdoc`; these are only used internally.

## License

This library is licensed under [Apache 2.0](https://www.apache.org/licenses/LICENSE-2.0).
